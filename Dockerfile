FROM golang:alpine AS base
RUN wget -O /usr/bin/alpine-bootstrap.sh https://gitlab.com/usvc/images/ci/base/raw/master/shared/alpine-bootstrap.sh \
  && chmod +x /usr/bin/alpine-bootstrap.sh \
  && /usr/bin/alpine-bootstrap.sh \
  && rm -rf /usr/bin/alpine-bootstrap.sh
RUN apk add --no-cache g++
WORKDIR /
LABEL description="An image for handling golang builds in the CI pipeline"
LABEL canonical_url="https://gitlab.com/usvc/images/ci/golang"
LABEL license="MIT"
LABEL maintainer="zephinzer"
LABEL authors="zephinzer"

FROM base AS docker
RUN wget -O /usr/bin/docker-bootstrap.sh https://gitlab.com/usvc/images/ci/docker/raw/master/shared/docker-bootstrap.sh \
  && chmod +x /usr/bin/docker-bootstrap.sh \
  && /usr/bin/docker-bootstrap.sh \
  && rm -rf /usr/bin/docker-bootstrap.sh
VOLUME [ "/var/run/docker.sock" ]

FROM docker AS gitlab
# for more info, see:
#   https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
ENV DOCKER_HOST=tcp://docker:2375/ \
  DOCKER_DRIVER=overlay2
